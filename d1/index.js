console.log("hello world")
/*
	Javascript can also command our browsers to perform different arithmetic operations just like how mathematics work
*/
// ARITHMETIC OPERATORS SECTION

// creation of variables to used in mathematics operations
let x = 1397;
let y = 7831;
/*
	basic operators
		+ addition operator
		- subtraction operator
		* mutiplication operator
		/ division operator
		% modulo operator
*/
let sum = x + y;
console.log(sum);

let difference = x - y;
console.log(difference);

let product = x * y;
console.log(product);

let quotient = x / y;
console.log(quotient);

let remainder = y % x;
console.log(remainder);

// Assignment Operator
// = assignment operator and it is used to assign a value to a variable; the value on the right side of the operator is assigned to the left variable
let assignmentNumber = 8;

// addition assignment operator - adds the value of the right operand to a variable and assigns the result to the variable
// assignmentNumber = assignmentNumber + 2;
// shorthand for addition assignment(+=)
assignmentNumber +=  2;
console.log(assignmentNumber);

// subtraction assignment operator
assignmentNumber -=  2;
console.log(assignmentNumber);

// multiplication assignment operator
assignmentNumber *=  2;
console.log(assignmentNumber);

// division assignment operator
assignmentNumber /=  2;
console.log(assignmentNumber);

// Multiple Operators and Parenthesis

/*
	When multiple operators are present in a single statement, it follows the PEMDAS (Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)
*/
/*
	MDAS
	the code below is computed based on the following:
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
*/
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

/*
	pemdas
	the code below is computed based on the following:
		1. 2 - 3 = -1
		2. -1 * 4 = -4
		3. -4 / 5 = -0.8
		4. 1 + -0.8 = 0.2
*/
let pemdas = 1 + (2 - 3) * 4 / 5;
console.log(pemdas);

/*
	adding another set of parenthesis to create a more complex computation would still follow the same rule:
		1. 4 / 5 = 0.8
		2. 2 - 3 = .1
		3. 1+ -1 = 0
		4. 0 * 0.8 = 0
*/
pemdas = (1 + (2 - 3)) * (4 / 5);
console.log(pemdas);


// Increment and Decrement Section
// assigning a value to a variable to be used in increment and decrement section
let z = 1;
/*
	increment ++ = adding 1 to the value of the variable whether before or after the assigning of value3
		pre-increment (++z) is adding 1 to the value before it is assigned to the variable
		post-increment (z++) is adding 1 to the value after it is assigned to the variable
*/
let increment = ++z
// the value of z is added by a value of 1 before returning the value and storing it inside the variable
console.log("Result of pre-increment: " + increment);
// the of z was also increased by 1 even though we didn't explicitly specify any value reassignment
console.log("Result of pre-increment: " + z);

increment = z++;
// the value of z is at 2 before it was incremented
console.log("Result of post-increment: " + increment);
// the value of z was increased again reassigning the value to 3
console.log("Result of post-increment: " + z);

/*
	decrement -- subtracting 1 to the value whether before or after assigning it to the variable
		pre-decrement (--z) is subtracting 1 to the value before it is assigned to the variable
		post-increment (z--) is subtracting 1 to the value after it is assigned to the variable
*/
let decrement = --z;
// the value of z is at 3 before it was decremented
console.log("Result of pre-decrement: " + decrement);
// the value of z was reassigned to 2
console.log("Result of pre-decrement: " + z);


decrement = z--;
// the value of z was 2 before it was decremented
console.log("Result of pre-decrement: " + decrement);
// the value of z was decreased and reassigned to 1
console.log("Result of pre-decrement: " + z);

// Type Coercion
/*
	is the automatic or implicit conversion of values from one data type to another
	this happens when operations are performed on different data types that would normally not be possible and yield irregular result
	values are automatically assigned/converted from one data type to another in order to resolve operations
*/

let numbA = '10';
let numbB = 12;
/*
	num + string
		resulting data type is a string 
		the value of numbA, although it is technically a number, since it is a string data type, it cannot be included in any mathematical operation
*/
let coercion = numbA + numbB;
console.log(coercion);

/*
try to have type coercion for the following

-number data + number data = int/number
-boolean + number = int/number
-boolean + string = string
*/
// num + num
let numbC = 16;
let numbD = 14;
// non coercion happens when the resulting data type is not really different from both of the original data types
let nonCoercion = numbC + numbD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// boolean + num
let numbE = true + 1;
/*
	results into a number data type
	boolean is just like binary in javascript
		true = 1
		false = 0
*/
console.log(numbE);
console.log(typeof numbE);

let varA = "String plus " + true;
console.log(varA);
console.log(typeof varA);


// Comparison Operators

// equality operator
/*
	checks whether the operands are equal/have the same content
	attempts to convert and compare operands of different data types
	returns boolean value
*/
console.log(1 == 1);
console.log(1 == 2);

console.log(1 == "1");
console.log(1 == true);
console.log('juan' == 'juan');
let juan = 'juan';
console.log('juan' == juan);

// inequality operator
/*
	checks whether the operands are inequal/does not have the same content
	attempts to convert and compare operands of different data types
	returns boolean value
*/
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log("juan" != 'juan');
console.log('juan' != juan);

// Strict equality/inequality operators
/*
	checks the content of the operands
	it also checks/compares the data types of the 2 operands
	JS is a loosely typed language, meaning that the values of different data type can be stored inside a variable

	strict operators are better to be used in most cases to ensure that the data types provided are correct
*/
// Strict equality operator
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log("juan" === 'juan');
console.log('juan' === juan);

// Strict inquality operator
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log("juan" !== 'juan');
console.log('juan' !== juan);

// Relational operators
// some comparison operators check whether one value is greater or less than to the other value
// just like equality and inequality operators, they return boolean based on the assessment of the two values

let a = 50;
let b = 65;
// GT/greater than >
let greaterThan = a > b;
// LT/less than <
let lessThan = a < b;
// GTE/greater than or equal to >=
let greaterThanOrEqualTo = a >= b;
// LTE/less than or equal to <=
let lessThanOrEqualTo = a <= b;

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);

// true - product of a forced coercion to change the string into a number data type
let numStr = "30";
console.log(a > numStr);

// since a string is not numeric, the string was converted into a number and it resulted into NaN 65 is not greater than NaN
	// NaN - Not a Number; is the result of unsuccessful conversion of string into number data type of an alphanumeric string.
let str = "twenty";
console.log(b >= str);

// Logical Operators
/*
	checking whether the values of the two or more variables are true/false
*/

let isLegalAge = true;
let isRegistered = false;

// And Operator (&&)
// returns true if all values are true
/*
	1                      2                        end result
	true                 true                   true
	true                 false                  false
	false                true                   false
	false                false                  false
*/
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of And Operator: " + allRequirementsMet);


// Or Operator (||)
// returns true if one of values is true
/*
	1                      2                        end result
	true                 true                   true
	true                 false                  true
	false                true                   true
	false                false                  false
*/
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Or Operator: " + someRequirementsMet);

// Not Operator (!)
// returns the opposite of the value
/*
	!true = false
	!false = true
*/
let someRequirementsNotMet = !isRegistered;
console.log("Result of Not Operator: " + someRequirementsNotMet);